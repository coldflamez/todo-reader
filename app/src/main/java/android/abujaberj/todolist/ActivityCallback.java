package android.abujaberj.todolist;

import android.net.Uri;

import java.util.ArrayList;

public interface ActivityCallback {
    void onPostSelected(int pos);
}
