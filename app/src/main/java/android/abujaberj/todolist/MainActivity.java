package android.abujaberj.todolist;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;

public class MainActivity extends SingleFragmentActivity implements ActivityCallback {
    public ArrayList<android.abujaberj.todolist.Todoitem> todoitems = new ArrayList<Todoitem>();
    public int currentItem;


    @Override
    protected Fragment createFragment() {
        return new Todofrag();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment);
    }

    @Override
    public void onPostSelected(int pos) {
        int currentItem = pos;
        Fragment newFragment = new itemViewFragment();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
}