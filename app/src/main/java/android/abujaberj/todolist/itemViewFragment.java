package android.abujaberj.todolist;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

public class itemViewFragment extends Fragment {

    private RecyclerView recyclerView;
    private ActivityCallback activityCallback;
    private MainActivity activity;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = (MainActivity)activity;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View layoutView = inflater.inflate(R.layout.itemview, container, false);

        TextView tv1 = (TextView) layoutView.findViewById(R.id.tv1);
        TextView tv2 = (TextView) layoutView.findViewById(R.id.tv2);
        TextView tv3 = (TextView) layoutView.findViewById(R.id.tv3);
        TextView tv4 = (TextView) layoutView.findViewById(R.id.tv4);



        tv1.setText("Title" + activity.todoitems.get(activity.currentItem).title);
        tv2.setText("Date Add" + activity.todoitems.get(activity.currentItem).dateAdd);
        tv3.setText("Date Due" + activity.todoitems.get(activity.currentItem).dateDue);
        tv4.setText("Category" + activity.todoitems.get(activity.currentItem).category);

            return layoutView;
        }
}