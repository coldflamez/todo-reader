package android.abujaberj.todolist;

import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class TodoAdapter extends RecyclerView.Adapter<TodoHolder> {
    ArrayList<Todoitem> todoitems;
    ActivityCallback activityCallback;

    public TodoAdapter(ActivityCallback activityCallback, ArrayList<Todoitem> todoitems){
        this.todoitems = todoitems;
        this.activityCallback = activityCallback;
    }

    @Override
    public TodoHolder onCreateViewHolder(ViewGroup parent, int i){
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(android.R.layout.simple_list_item_1,parent, false);
        return new TodoHolder(view);
    }

    @Override
    public void onBindViewHolder(TodoHolder holder, final int position) {
        holder.titleText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activityCallback.onPostSelected(position);

            }
        });

        holder.titleText.setText(todoitems.get(position).title);
    }
        @Override
        public int getItemCount() {
            return todoitems.size();

    }
}
