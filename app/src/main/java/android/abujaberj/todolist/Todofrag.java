package android.abujaberj.todolist;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import static android.support.v7.widget.RecyclerView.*;

public class Todofrag extends Fragment {

    private RecyclerView recyclerView;
    private ActivityCallback activityCallback;
    private MainActivity activity;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        activityCallback = (ActivityCallback)activity;
        this.activity = (MainActivity)activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        activityCallback = null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.todolistfragment, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        Todoitem i1 = new Todoitem("BMW", "11/01/99", "11/02/99", "GermanCar");
        Todoitem i2 = new Todoitem("Honda", "12/01/99", "12/02/99", "JapaneseCar");
        Todoitem i3 = new Todoitem("Gmc", "01/02/00", "01/03/00", "AmericanCar");
        Todoitem i4 = new Todoitem("Ferrari", "02/01/00", "02/02/00", "ItalianCar");

        activity.todoitems.add(i1);
        activity.todoitems.add(i2);
        activity.todoitems.add(i3);
        activity.todoitems.add(i4);

        updateUserInterface();

        return view;
    }

    public void updateUserInterface(){
        TodoAdapter adapter = new TodoAdapter(activityCallback, activity.todoitems);
        recyclerView.setAdapter(adapter);
    }
}
